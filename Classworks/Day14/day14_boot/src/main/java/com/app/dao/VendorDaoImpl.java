package com.app.dao;

import java.util.List;

import javax.persistence.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Role;
import com.app.pojos.Vendor;

@Repository // spring bean : data access logic
public class VendorDaoImpl implements IVendorDao {
	// DAO : dependent obj , dependency : 
	//javax.persistence.EntityManager (equivalent to org.hibernate.Session)
	@PersistenceContext
	private EntityManager manager;
	@Override
	public Vendor authenticateUser(String email, String password) {
		Vendor v = null;
		String jpql = "select v from Vendor v  where v.email=:em and v.password=:pass";

		v = manager.createQuery(jpql, Vendor.class).setParameter("em", email)
				.setParameter("pass", password).getSingleResult();

		return v;// dao layer is returning PERSISTENT vendor pojo to service
	}

	@Override
	public List<Vendor> listAllVendors() {
		String jpql="select v from Vendor v where v.userRole=:role";
		return manager.
				createQuery(jpql,Vendor.class).setParameter("role", Role.VENDOR).getResultList();
	}

}
