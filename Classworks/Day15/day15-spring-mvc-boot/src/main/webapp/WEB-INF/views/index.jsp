<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%--  importing spring supplied JSP tag lib --%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%--URL rewriting if needed n makes all URI relative to root of curnt web app  --%>
	<h5>
		<a href="<spring:url value='/user/login'/>">User Login</a>
	</h5>
	<h5>
		<a href="<spring:url value='/product/show/123/bread/50/2020-12-10'/>">Show Product Details</a>
	</h5>
</body>
</html>