package com.app.service;

import java.util.List;

import com.app.pojos.Product;

public interface IProductService {
//add a method to List all Products
  List<Product> getAllProducts();
  //add a method to get specific product details by its id
  Product getProductDetails(int productId);
  //Get product details  by supplied name
  Product getProductDetailsByName(String pName);
  //add new product details
  Product addProductDetails(Product p);//p : transient
//update product details
  Product updateProductDetails(Product p);//p : detached
  //delete product details
  void deleteProductDetails(int productId);
}
