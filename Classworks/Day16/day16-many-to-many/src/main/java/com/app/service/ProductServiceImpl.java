package com.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ProductNotFoundException;
import com.app.dao.ProductRepository;
import com.app.pojos.Product;

@Service //mandatory
@Transactional //optional since it's by default already added on JpaRepository
public class ProductServiceImpl implements IProductService {
	//dependency
	@Autowired
	private ProductRepository productRepo;

	@Override
	public List<Product> getAllProducts() {
		// TODO Auto-generated method stub
		return productRepo.findAll();
	}

	@Override
	public Product getProductDetails(int productId) {
		//invoke dao's method
		Optional<Product> optionalProduct = productRepo.findById(productId);
		if(optionalProduct.isPresent())
			return optionalProduct.get();
		//if product is not found : throw custom exception
		throw new ProductNotFoundException("Product NOt Found : Invalid ID "+productId);
	}
	

}
