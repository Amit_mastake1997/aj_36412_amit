package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IVendorDao;
import com.app.pojos.Vendor;

@Service //Mandatory annotation to tell SC whatever follows contains B.L
@Transactional//Mandatory : annotation to tell SC , to use tx mgr bean for
//automatically handling txs.
public class VendorServiceImpl implements IVendorService {
	//dependency : DAO layer
	@Autowired
	private IVendorDao vendorDao;

	@Override
	public Vendor authenticateUser(String email, String password) {
		// simply invoke dao's method for user authentication
		return vendorDao.authenticateUser(email, password);
	}

	@Override
	public List<Vendor> listAllVendors() {
		// TODO Auto-generated method stub
		return vendorDao.listAllVendors();
	}

	@Override
	public String deleteVendorDetails(int vendorId) {
		// TODO Auto-generated method stub
		return vendorDao.deleteVendorDetails(vendorId);
	}

	@Override
	public String registerNewVendor(Vendor vendor) {
		// TODO Auto-generated method stub
		return vendorDao.registerNewVendor(vendor);
	}//JPA implementor(Hibernate) auto cirty chking : insert : entity manager : closed
	//After disabling OSIV

	@Override
	public Vendor getVendorDetails(int vendorId) {
		// TODO Auto-generated method stub
		return vendorDao.getVendorDetails(vendorId);
	}

	@Override
	public String updateVendor(Vendor vendor) {
		// TODO Auto-generated method stub
		return vendorDao.updateVendor(vendor);
	}
		
	

}
