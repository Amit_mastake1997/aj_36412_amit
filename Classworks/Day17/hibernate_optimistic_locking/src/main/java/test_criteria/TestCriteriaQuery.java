package test_criteria;

import static utils.HibernateUtils.getSf;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import pojos.Stock;

//Criteria Queries used mainly as dynamic queries generated as a result 
//of different filtering criterias chosen by user at runtime
public class TestCriteriaQuery {

	public static void main(String[] args) {
		try (SessionFactory sf = getSf()) {
			Session hs = sf.getCurrentSession();
			Transaction tx = hs.beginTransaction();
			try {
				// A builder to construct criteria queries
				CriteriaBuilder builder = hs.getCriteriaBuilder();
				// Create CriteriaQuery object with specified result type
				CriteriaQuery<Stock> query = builder.createQuery(Stock.class);
				// Create a query root in the from clause equivalent to JPQL : (select s from
				// Stock s)
				Root<Stock> root = query.from(Stock.class);
				query.select(root);
				// getting all stocks
				System.out.println("All Stocks");
				hs.createQuery(query).getResultList().forEach(System.out::println);
				System.out.println("All Stocks with price > 1200");
				// In the JSON payload (request data) , if user has selects a filter by price , with min price of the
				// stock as 1200
				query.select(root).where(builder.gt(root.get("price"), 1200));
				hs.createQuery(query).getResultList().forEach(System.out::println);
				// In the JSON payload (request data) , if user has selects a filter by company name : reliance
				System.out.println("All Stocks by Company Name 'reliance' ");
				query.select(root).where(builder.equal(root.get("company"), "reliance"));
				hs.createQuery(query).getResultList().forEach(System.out::println);
				tx.commit();
			} catch (RuntimeException e) {
				tx.rollback();
				throw e;
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
