package dao;

import java.util.Date;
import java.util.List;

import pojos.Role;
import pojos.User;

public interface IUserDao {
//add a method for user registration
	String registerUser(User user);
	//add a method to fetch user details by its id
	User fetchUserDetails(int userId);
	//add a method to fetch details of all users
	List<User> fetchAllUserDetails();
	//add method to fetch selected user details
	List<User> fetchSelectedUserDetails(Date strtDate,Date endDate,Role userRole);
	//Objective : Display all user names registered between strt date n end date & under a specific role
	List<String> fetchSelectedUserNames(Date strtDate,Date endDate,Role userRole);
}
