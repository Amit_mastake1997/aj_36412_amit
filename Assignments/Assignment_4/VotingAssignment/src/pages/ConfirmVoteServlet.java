package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import dao.VoterDaoImpl;
import pojos.Candidate;
import pojos.Voter;

/**
 * Servlet implementation class ConfirmVoteServlet
 */
@WebServlet("/confirm_vote")
public class ConfirmVoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");

		try (PrintWriter pw = response.getWriter()) {
			HttpSession session = request.getSession();
			CandidateDaoImpl candidateDao = (CandidateDaoImpl) session.getAttribute("candidate_dao");
			VoterDaoImpl voterDao = (VoterDaoImpl) session.getAttribute("voter_dao");
			Voter user = (Voter) session.getAttribute("voter_details");
			String chosenCandidate = request.getParameter("can_name");
			int candidateId = 0;
			List<Candidate> candidates = candidateDao.listCandidates();
			for (Candidate candidate : candidates) {
				if(chosenCandidate.equals(candidate.getName()))
					candidateId = candidate.getId();
			}
			int voteIncrement = candidateDao.incrementVotes(candidateId);
			int votingstatus = voterDao.updateVotingStatus(user.getId());

			if(voteIncrement == 1 && votingstatus == 1)
			{
				pw.print("<h4>Candidate voted : " + chosenCandidate + "</h4>");
				pw.print("<h4>Thanks For Voting...</h4>");

			} else
				pw.print("<h4>No Cookie, Session Tracking Failed....</h4>");
		} catch (Exception e) {
			throw new ServletException("Error in do-get of : " + getClass().getName(), e);
		}
	}

}
