package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import pojos.Candidate;
import pojos.Voter;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/admin")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private List<Candidate> candidates = null;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			pw.print("<h5>Admin Successfully Loged in...</h5>");
			HttpSession session = request.getSession();
			
			System.out.println("From login page HS " + session.isNew());
			System.out.println("Session ID : " + session.getId());
			
			Voter user = (Voter) session.getAttribute("voter_details");
			CandidateDaoImpl candidateDao = (CandidateDaoImpl) session.getAttribute("candidate_dao");
			
			if(user != null) {
				candidates = candidateDao.voteCounting();
				pw.print("<h4>Hello, " + user.getName() + " </h4>");
				
				pw.print("<h4>Vote Counts : </h4>");
				pw.print("<table  style='background-color: cyan; margin: auto;' border='1'>");
				pw.print("<tr>");
				pw.print("<td>" + "<h4>Candidate Name</h4>" + "</td>");
				pw.print("<td>" + "<h4>Candidate Party</h4>" + "</td>");
				pw.print("<td>" + "<h4>Votes</h4>" + "</td>");
				pw.print("</tr>");
				for (Candidate candidate : candidates) {
					pw.print("<tr>");
					pw.print("<td>" + candidate.getName() + "</td>");
					pw.print("<td>" + candidate.getParty() + "</td>");
					pw.print("<td>" + candidate.getVotes() + "</td>");
					pw.print("</tr>");
				}
				
				pw.print("</table>");
			}
			else
				pw.print("<h4>No Cookies , Session Tracking failed!!!!!!!</h4>");
			pw.print("<h5><a href='logout'>Log Me Out</a></h5>");
		
		} catch (Exception e) {
			throw new ServletException("Error in do-get of : " + getClass().getName(), e);
		}
	}

}
