package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import dao.VoterDaoImpl;
import pojos.Voter;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet(urlPatterns = "/authenticate", loadOnStartup = 1)
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	VoterDaoImpl voterDao = null;
	CandidateDaoImpl candidateDao = null;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		try {
			voterDao = new VoterDaoImpl();
			candidateDao = new CandidateDaoImpl();
		} catch (Exception e) {
			throw new ServletException("Error in init of " + getClass().getName(), e);
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
			voterDao.cleanUp();
			candidateDao.cleanUp();
		} catch (Exception e) {
			throw new RuntimeException("In destroy of " + getClass().getName() + " Error ", e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			String email = request.getParameter("em");
			String password = request.getParameter("pass");
			System.out.println(email + " " + password);
			Voter user = voterDao.validateUser(email, password);
			
			if (user == null)
				pw.print("<h5>Invalid Login , Please <a href='login.html'>Retry</a></h5>");
			else {
				HttpSession session = request.getSession();

				System.out.println("From login page HS " + session.isNew());
				System.out.println("Session ID : " + session.getId());
				session.setAttribute("voter_details", user);
				session.setAttribute("candidate_dao", candidateDao);
				session.setAttribute("voter_dao", voterDao);
				if (user.getRole().equals("admin"))
					response.sendRedirect("admin");
				else if (user.getRole().equals("voter")) {
					if (user.getStatus() == 1)
						response.sendRedirect("voted");
					else if (user.getStatus() == 0)
						response.sendRedirect("tovote");
				}
			}
		} catch (Exception e) {
			throw new ServletException("Error in do-post of " + getClass().getName(), e);
		}
	}

}
