package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojos.Voter;

/**
 * Servlet implementation class VotedServlet
 */
@WebServlet("/voted")
public class VotedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			HttpSession session = request.getSession();
			Voter user = (Voter) session.getAttribute("voter_details");
			if(user != null) {
				pw.print("<h4>Voter Details : " + user.toString() + "</h4>");
				pw.print("<h4>You have already voted...</h4>");

			} else
				pw.print("<h4>No Cookies , Session Tracking failed!!!!!!!</h4>");
			pw.print("<h5><a href='logout'>Log Me Out</a></h5>");
		}
	}

}
