package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import dao.CandidateDaoImpl;
import pojos.Candidate;
import pojos.Voter;

/**
 * Servlet implementation class ToVoteServlet
 */
@WebServlet("/tovote")
public class ToVoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try (PrintWriter pw = response.getWriter()) {
				
			HttpSession session = request.getSession();
			System.out.println("From category page HS " + session.isNew());
			System.out.println("Session ID : " + session.getId());
			
			Voter user = (Voter) session.getAttribute("voter_details");
			CandidateDaoImpl candidateDao = (CandidateDaoImpl) session.getAttribute("candidate_dao");
			List<Candidate> candidates = candidateDao.listCandidates();
			if(user != null) {
				pw.print("<h4> Hello,  " + user.getName() + "</h4>");
				pw.print("<form action='confirm_vote'>");
				pw.print("Choose Candidate ");
				pw.print("<select name='can_name'>");
				for (Candidate candidate : candidates) 
					pw.print("<option value=" + candidate.getName() + ">" + candidate.getName() + "</option>");
				pw.print("</select><br>");
				pw.print("<input type='submit' value='Choose'>");
				pw.print("</form>");

			} else {
				pw.print("<h4>No Cookies , Session Tracking failed!!!!!!!</h4>");
				System.out.println("if");
			}
		}catch(

	Exception e)
	{
		throw new ServletException("Error in do-post of " + getClass().getName(), e);
	}
}

}
