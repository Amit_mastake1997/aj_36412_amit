package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import static utils.DBUtils.fetchConnection;

import pojos.Candidate;

public class CandidateDaoImpl implements ICandidateDao {

	private Connection connection = null;
	private PreparedStatement selectStatement;
	private PreparedStatement updateVotes;
	private PreparedStatement countingVotes;
	
	public CandidateDaoImpl() throws Exception {
		connection = fetchConnection();
		selectStatement = connection.prepareStatement("Select * from candidates");
		updateVotes = connection.prepareStatement("Update candidates set votes= votes+1 where id=?");
		countingVotes = connection.prepareStatement("select * from candidates order by votes desc");
		System.out.println("Candidate dao created...");
	}
	
	@Override
	public List<Candidate> listCandidates() throws Exception {
		List<Candidate> candidates = new ArrayList<Candidate>();
		try (ResultSet rs = this.selectStatement.executeQuery() ) {
			while( rs.next())
				candidates.add(new Candidate(rs.getInt("id"), rs.getString("name"), rs.getString("party"), rs.getInt("votes")));
		}
		return candidates;
	}

	public void cleanUp() throws Exception
	{
		if(selectStatement != null)
			selectStatement.close();

		if(connection != null)
			connection.close();
		System.out.println("Candidate dao cleaned up...");
	}

	@Override
	public int incrementVotes(int candidateId) throws Exception {
		this.updateVotes.setInt(1, candidateId);
		return this.updateVotes.executeUpdate();
	}

	@Override
	public List<Candidate> voteCounting() throws Exception {
		List<Candidate> candidates = new ArrayList<Candidate>();
		try (ResultSet rs = this.countingVotes.executeQuery() ) {
			while( rs.next())
				candidates.add(new Candidate(rs.getInt("id"), rs.getString("name"), rs.getString("party"), rs.getInt("votes")));
		}
		return candidates;
	}
}
