package dao;

import static utils.DBUtils.fetchConnection;

import java.sql.*;

import pojos.Voter;

public class VoterDaoImpl implements IVoterDao {

	private Connection connection = null;
	private PreparedStatement selectStatement;
	private PreparedStatement updateStatus;
	
	public VoterDaoImpl() throws Exception {
		connection = fetchConnection();
		selectStatement = connection.prepareStatement("select * from voters where email=? and password=?");
		updateStatus = connection.prepareStatement("Update voters set status=1 where id=?");
		System.out.println("Voter dao created...");
	}
	@Override
	public Voter validateUser(String email, String password) throws Exception {
		selectStatement.setString(1, email);
		selectStatement.setString(2, password);
		try(ResultSet rs = selectStatement.executeQuery())
		{
			if(rs.next())
				return new Voter(rs.getInt(1), rs.getString(2), email, password, rs.getInt(5), rs.getString(6));
		}
		return null;
	}

	public void cleanUp() throws Exception
	{
		if(selectStatement != null)
			selectStatement.close();

		if(connection != null)
			connection.close();
		System.out.println("Voter dao cleaned up...");
	}
	@Override
	public int updateVotingStatus(int voterId) throws Exception {
		this.updateStatus.setInt(1, voterId);
		return this.updateStatus.executeUpdate();
	}
}
