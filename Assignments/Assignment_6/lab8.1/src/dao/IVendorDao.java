package dao;

import java.time.LocalDate;
import java.util.List;

import pojos.Vendor;

public interface IVendorDao {

	String registerVendor(Vendor v);
	List<Vendor> listSpecificVendors(LocalDate regDate,double amount);
	//Offer discount to all vendors registered before specific date
	List<Vendor> applyDiscount(double discount,LocalDate date);
}
