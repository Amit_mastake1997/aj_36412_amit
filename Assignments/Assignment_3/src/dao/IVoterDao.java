package dao;

import pojo.Voter;

public interface IVoterDao {

	Voter ValidateUser(String email,String pwd) throws Exception;
}