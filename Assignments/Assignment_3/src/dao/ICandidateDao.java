package dao;

import java.util.List;

import pojo.Candidate;

public interface ICandidateDao {

	List<Candidate> listCandidates() throws Exception;
}