package pojo;

public class Candidate {
	private int CnadidateId;
	private String name;
	private String party;
	private int votes;
	public Candidate(int cnadidateId, String name, String party, int votes) {
		
		CnadidateId = cnadidateId;
		this.name = name;
		this.party = party;
		this.votes = votes;
	}
	public int getCnadidateId() {
		return CnadidateId;
	}
	public void setCnadidateId(int cnadidateId) {
		CnadidateId = cnadidateId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParty() {
		return party;
	}
	public void setParty(String party) {
		this.party = party;
	}
	public int getVotes() {
		return votes;
	}
	public void setVotes(int votes) {
		this.votes = votes;
	}
	@Override
	public String toString() {
		return "Candidate [CnadidateId=" + CnadidateId + ", name=" + name + ", party=" + party + ", votes=" + votes
				+ "]";
	}
}
