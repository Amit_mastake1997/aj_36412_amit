package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.VoterDao;
import pojo.Voter;
@WebServlet(urlPatterns = "/authenticate", loadOnStartup = 1)
public class LoginVoter extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VoterDao dao;
	public void init() throws ServletException {
		// create dao instance
		try {
			dao = new VoterDao();
		} catch (Exception e) {
			// Centralized err handling in servlets
			// Inform WC that init has failed : so that WC won't continue with remaining
			// life cycle of the servlet
			// HOW : throw servlet exc to WC
			//throw new ServletException("err in init of " + getClass().getName(), e);
              System.out.println("Invalide errors");
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
			dao.cleanUp();
		} catch (Exception e) {
			// System.out.println("in destroy of "+getClass().getName()+" err "+e);
			throw new RuntimeException("in destroy of " + getClass().getName() + " err ", e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// set cont type
		response.setContentType("text/html");
		// get PW
		try (PrintWriter pw = response.getWriter()) {
			// get request params : email n password
			String email = request.getParameter("em");
			String password = request.getParameter("pass");
			// servlet ---> DAO's method for customer validation
			Voter voter = dao.ValidateUser(email, password);
			if (voter == null) // => invalid login --send a mesg n link to retry
				pw.print("<h5>Invalid Login , Please <a href='login.html'>Retry</a></h5>");
			else // => valid login --send a mesg + customer details

			{
				// get HttpSession object from WC
				HttpSession session = request.getSession();
				// confirm if session is new or old
				System.out.println("From login page HS " + session.isNew());
				// session id
				System.out.println("Session ID " + session.getId());
				// save validated user details under session scope
				session.setAttribute("user_details", voter);
				// do u want to share any thing more across other dyn web pages ?
				// YES : dao instances , empty cart : ArrayList<Integer> : list of selected book
				// ids.
				session.setAttribute("cust_dao", dao);
				session.setAttribute("Voter_list", new ArrayList<Integer>());
				// In case of successful login : navigate the clnt to the next page in NEXT
				// request coming from the clnt browser
				response.sendRedirect("voterSatus");// WC sends immediate temp redirect resp
				// SC 302 | location = http://host:port/day2.1/category, set-cookie : cookie
				// name : JSESSIONID value : fsfhgf657567
				// details | body EMPTY
				// WC throws IllegalStateException , if u call send Redirect after committing
				// the response.

			}

		} catch (Exception e) {
			// inform WC
			throw new ServletException("err in do-post of " + getClass().getName(), e);
		}
	}

}