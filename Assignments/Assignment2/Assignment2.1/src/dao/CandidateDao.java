package dao;
import static utils.DBUtils.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import pojo.Candidate;

public class CandidateDao implements ICandidateDao {
	private Connection conn;
	private PreparedStatement pst1;
	
	public CandidateDao() throws Exception {
		conn=getConnection();
		pst1=conn.prepareStatement("select * from candidates");
		System.out.println("Candidate dao created...");
	}
	@Override
	public List<Candidate> listCandidates() throws Exception {
		List<Candidate> candidates = new ArrayList<Candidate>();
		try( ResultSet rs = pst1.executeQuery( ) ){
			while( rs.next()) {
				Candidate candi = new Candidate(rs.getInt("id"),rs.getString("name"),rs.getString("party"),rs.getInt("votes"));
				candidates.add( candi );
			}
		}
		return candidates;

	}

}