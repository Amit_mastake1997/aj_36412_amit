package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import pojo.Voter;
import static utils.DBUtils.getConnection;
public class VoterDao implements IVoterDao {
	private Connection conn;
	private PreparedStatement pst1;
	
	public VoterDao() throws Exception {
		conn=getConnection();
		pst1=conn.prepareStatement("select * from voters where email=? and password=?");
		System.out.println("Voter dao created...");
	}

	@Override
	public Voter ValidateUser(String email, String pwd) throws Exception {
		pst1.setString(1, email);
		pst1.setString(2, pwd);	
		try(ResultSet rs=pst1.executeQuery())
		{
			if(rs.next())
			{
				return new Voter(rs.getInt(1),rs.getString(2),email,pwd,rs.getBoolean(5),rs.getString(6));
			}
			return null;
		}
	}
	public void cleanUp() throws Exception {
		if(pst1 !=null)
			pst1.close();
		if(conn!=null)
			conn.close();
		System.out.println("Customer dao closed...");
	}

}