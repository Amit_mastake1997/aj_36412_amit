package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Authentication servlet", urlPatterns = { "/authenticate" })
public class inputServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set resp content type
		response.setContentType("text/html");
		//open PW to send resp from servlet ---> clnt
		try(PrintWriter pw=response.getWriter())
		{
			pw.print("<h5>User_id : "+request.getParameter("f1")+"</h5>");
			pw.print("<h5>Favourite color  : "+request.getParameter("clr")+"</h5>");
			pw.print("<h5>browser : "+request.getParameter("browser")+"</h5>");
			pw.print("<h5>City  : "+request.getParameter("myselect")+"</h5>");
			pw.print("<h5>Details : "+request.getParameter("info")+"</h5>");
		}
	}

}