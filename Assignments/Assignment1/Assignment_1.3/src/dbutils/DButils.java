package dbutils;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class DButils {
	private static Properties p;
	static {
		try {
			FileInputStream inputStream = new FileInputStream("config.properties");
			p = new Properties();
			p.load(inputStream);
			Class.forName(p.getProperty("DRIVER"));
		} catch (Exception cause) {
			throw new RuntimeException(cause);
		} 
	}
	public static Connection getConnection( ) throws SQLException{
		return DriverManager.getConnection(p.getProperty("URL"), p.getProperty("USER"), p.getProperty("PASSWORD"));		
	}
}