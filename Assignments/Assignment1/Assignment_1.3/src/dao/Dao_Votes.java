package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import dbutils.DButils;

public class Dao_Votes implements Closeable{
	Connection connection=null;
	PreparedStatement smtselect=null;
	PreparedStatement smtinsert=null;
	PreparedStatement smtdelete=null;
	PreparedStatement smtupdate=null;
	 
	public Dao_Votes() throws SQLException {
		// TODO Auto-generated constructor stub
		this.connection=DButils.getConnection();
		this.smtselect=this.connection.prepareStatement("select * from voter");
		this.smtinsert=this.connection.prepareStatement("INSERT INTO voter(name,email,password,status,role) VALUES(?,?,?,?,?)");
		this.smtdelete = this.connection.prepareStatement("DELETE FROM voter WHERE id=?");
		//this.smtupdate = this.connection.prepareStatement("UPDATE voter SET rating=? WHERE name=?");
	}
	
	
	
	
	
	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		try {
			connection.close();
		}
		catch(SQLException cause) {
			throw new IOException(cause);
		}
	}
	

}
